//Si se usan más etiquetas html, esto dará errores si no se engloba en una.
//Por eso se usa un div en el template! Solo un padre
Vue.component('hello-world',{
    data() {
        return {
            titulo: 'Hola mundo mundial',
            mensaje: 'Un mensaje'
        }
    },
    template: `<div><h1>{{titulo}}</h1>
               <p>{{mensaje}}</p></div>`
});

