Vue.component('vue-directive', {
    template: `<div>
                    <h1 v-text="title"></h1>
                    <p v-text="texto"></p>
                    <a v-text="link.text" v-bind:href="link.href" v-bind:title="link.title"></a>
                    <directive-html></directive-html>
                    <otro-html></otro-html>
                    <directive-vshow></directive-vshow>
                    <directive-vif></directive-vif>
                    <directive-vfor></directive-vfor>
                    <directive-on></directive-on>
                    <directive-model></directive-model>
                    <directive-slot></directive-slot>
                </div>`,
    data() {
        return {
            title: 'Directivas de Vue.js',
            texto: 'Texto con directiva v-text',
            link: {
                text: 'VueJS Org',
                href: 'https://vuejs.org/',
                title: 'Documentacion de VueJS'
            }
        }
    },
    components: {
     'directive-html'   : directivehtml,
     'otro-html'        : otro,
     'directive-vshow'  : directiveshow,
     'directive-vif'    : directiveif,
     'directive-vfor'   : directivefor,
     'directive-on'     : directiveon,
     'directive-model'  : directivemodel,
     'directive-slot'   : directiveSlot
    }
});

//Explicación: como un elemento html se "llama" al elemento texto con el v-text del mismo nombre dentro del html
//Con v-bind uno instancia objetos a otros atributos de html, ejemplo el v-bind:href
//Estos son las directivas. Encuentre más en https://vuejs.org/v2/api/#Directives
