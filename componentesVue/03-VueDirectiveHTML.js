//Se pretende usar esto como un componente hijo del comonente de la hoja 02. Por eso en el html se declara este 
//primero y después el 02. Finalmente el objeto vue
var directivehtml= ('html-directive', {
    template: `<div>
                    <h1 v-text="title"></h1>
                    <p v-html="texto"></p>
                </div>`,
    data() {
        return {
            title: 'Directivas de Vue.js',
            texto: '<b>Hola desde el directory html</b>',
        }
    }
});

var otro= ('otro-html', {
    template: `<div>
                    <h3 v-text="title"></h3>
                    <p v-html="texto"></p>
                </div>`,
    data() {
        return {
            title: 'Directivas de Vue.js',
            texto: '<i>Hola desde el directory html</i>',
        }
    }
});

//Explicación: v-html es equivalente a un innerHTML en js. simplemente se encrista el elemento texto que es html dentro de p
//Estos son las directivas. Encuentre más en https://vuejs.org/v2/api/#Directives
