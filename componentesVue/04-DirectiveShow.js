var directiveshow= ('show-directive', {
    template: `<div>
                    <h2 v-text="title"></h2>
                    <p v-show="user.permission" v-html="texto"></p>
                    <p v-show="user.permission">El usuario tiene permiso v-show</p>
                    <p v-show="!user.permission">El usuario NO tiene permiso v-show</p>
                </div>`,
    data() {
        return {
            title: 'Directivas v-show',
            texto: '<b>Hola desde directive v-show</b>',
            mostrar: true,
            user:{
                permission: false
            }
        }
    }
});

//En js true: true, 1, otro elemento
//se puede usar por ejemplo user.permission para un estado logico y !user.permission para negar ese estado logico
