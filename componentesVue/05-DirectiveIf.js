var directiveif= ('if-directive', {
    template: `<div>
                    <h2 v-text="title"></h2>
                    <p v-if="user.permission" v-html="texto"></p>
                    <p v-if="user.permission && vip">El usuario tiene permiso v-if y es VIP</p>
                    <p v-else-if="!user.permission && vip">El usuario NO tiene permiso v-if pero es VIP</p>
                    <p v-else>El usuario NO tiene permiso v-if</p>
                </div>`,
    data() {
        return {
            title: 'Directivas v-if',
            texto: '<b>Hola desde directive v-if</b>',
            user:{
                permission: false
            },
            vip: true
        }
    }
});

//En js true: true, 1, otro elemento
//se puede usar por ejemplo user.permission para un estado logico y !user.permission para negar ese estado logico
//Opción mucho más segura que el v-show para datos más sensibles
/* 
<v-if verifica si se cumple la condicion>
<v-selse-if> se usa para encadenar if. Se llevan acabo si el if o else-if anterior son falsos
<v-else> debe declararse luego de un v-if y se ejecutará cuando v-if o los v-else-if anteriores sea false o 0. No requiere entrada

*/
