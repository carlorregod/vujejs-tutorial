var directivefor= ('for-directive', {
    template: `<div>
                    <h2 v-text="title"></h2>
                    <p v-html="texto"></p>
                    <p>Forma simple pero ineficiente</p>
                    <ol>
                        <li v-for="color in lista" v-text="color"></li>
                    </ol>
                    <p>Otra forma más eficiente para obtener el index</p>
                    <ol>
                        <li v-for="(color, index) in lista":key="index" v-bind:content="index" v-text="color"></li>
                    </ol>
                    <p>Con objetos</p>
                    <ul>
                        <li v-for="(datos, index) in lista_json":key="index" v-bind:content="index" v-text="datos"></li>
                    </ul>
                    <ul>
                        <li v-for="(item,key, index) in lista_json":key="index" v-bind:content="index">
                            {{key}}:{{item}}
                        </li>
                    </ul>
                    <p>Ultimo ejemplo recorriendo arrays con json</p>
                    <ul>
                        <li v-for="(elemento, index) in otra_lista":key="index" v-bind:content="index">
                            {{elemento.nombre}} {{elemento.apellido}} tiene {{elemento.edad}} años
                        </li>
                    </ul>
                </div>`,
    data() {
        return {
            title: 'Directivas v-for',
            texto: '<b>Hola desde directive v-if</b>',
            lista: ['azul', 'verde', 'rojo', 'blanco'],
            lista_json :{
                nombre:'Carlos',
                apellido:'Orrego',
                edad:37
            },
            otra_lista: [
                {
                    nombre:'Carlos',
                    apellido:'Orrego',
                    edad:37
                },
                {
                    nombre:'esperanza',
                    apellido:'Castro',
                    edad:22
                },
                {
                    nombre:'martina',
                    apellido:'granitos',
                    edad:33
                }
            ]
        }
    }
});

/* 
<v-for se emplea como un foreach que recorre un elemento tipo arreglo>
equivalentes... foreach(lis in lista) === v-for="lis in lista"
En esa misma línea se puede emplear el elemento lis
v-for="lis in lista" v-text="lis"
Se repetirá cada línea en donde se inserte el v-for
Los objetos json se recorren idénticamente a los array
*/
