var directiveon= ('on-directive', {
    template: `<div>
                    <h2 v-text="title" @mouseover="mensajeConsola"></h2>
                    <p v-html="texto"></p>
                    <button v-on:click="mostrarAlerta">Click!!</button>
                    <button v-on:click="mostrarAlerta2">Click opcion 2!!</button>
                </div>`,
    data() {
        return {
            title: 'Directivas v-on',
            texto: '<b>Hola desde directive v-on</b>',
            welcome: 'Bienvenido!!!! desde variable',
            mensaje1: 'Mensaje!!!'
        }
    },
    methods: {
        mostrarAlerta: ()=>{
            alert('Hola mundo xd');
        },
        mostrarAlerta2(){
            alert(this.welcome);
        },
        mensajeConsola(){ //Se mostrará al pasar el mouse encima dle titulo
            console.log(this.mensaje1);
        }
    }
});

/* 
v-on se aplica para detectar eventos 
v-on:click es similar a document.onclick o a $(document).click()....
Así se puede aplicar los click, hover, mouseover, blur, etc...
se puede utilizar como v-on:evento o como @evento
*/
