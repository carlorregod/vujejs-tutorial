var directivemodel = ('model-directive',{
    template: `<div>
                    <h2 v-text="title"></h2>
                    <p v-html="texto"></p>
                    <label v-for="(peli,index) in peliculas":key="index"><input type="checkbox" :value="peli" v-model="favoritas">{{peli}}</label>
                    <div v-show="favoritas.length>0">
                        <h2>Listado de peliculas favoritas</h2>
                        <ol v-for="fav in favoritas">
                            <li>{{fav}}</li>
                        </ol>   
                    </div> 
               </div>`,
    data() {
        return {
            title: 'Directivas v-model',
            texto: '<b>Hola desde directive v-model</b>',
            peliculas: ['peli1', 'peli2', 'peli3'],
            favoritas : []

        }
    },
});