var directiveSlot = ('model-directive',{
    template: `<div><br>
                    <button>
                        <slot-directive>
                            <template #slot1></template>
                            <template v-slot:slot2></template>
                        </slot-directive>
                    </button>
               </div>`,

    components: {
        slotDirective
    }
});

/* Los slots se usan como templates o plantillas que proienen de otro archivo al lamarlo
ver el archivo componentesVue/slots/09-slots.js */