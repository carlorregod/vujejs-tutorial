var slotDirective = ('slot-directive',{
    template:  `
        <slot name="slot1">Por default</slot>
        <slot name="slot2">Mundo</slot>
    `
});

/* Las plantillas se encierran dentro de las etiquetas <slot name="algo"></slot> 
y dentro del contenido del slot se programa la plantilla*/