var SaludarComp = ('formuario-vue',{
    template: `
            <div @click="upps">
                <h2>Saludo para tí</h2>
                <label for="nombre">Nombre:</label><input type="text" name="nombre" id="nombre" v-model="name">
                <label for="pais">Pais:</label><input type="text" name="pais" id="pais" v-model="pais">
                <button @click.stop="saludar">Saludar</button>
            </div>
                `,
    data(){
        return {
            name: '',
            pais: ''
        }
    },
    methods: {
        saludar(){
            alert('Hola mi nombre es '+this.name+' y soy de '+this.pais);
        },
        upps(){
            alert('No diste click en el botón');
        }
    }
});



Vue.component('vue-directive', {
    template: `<div>
                    <h1 v-text="title"></h1>
                    <p v-text="texto"></p>
                    <saludar-comp></saludar-comp>
                </div>`,
    data() {
        return {
            title: 'Ejercicio 1',
            texto: 'Solucionando ejercicio 1',
        }
    },
    components: {
     'saludar-comp'   : SaludarComp,
    }
});




var vm = new Vue({
    el:'.lista_vue',
});