Vue.component('componente-principal',{
    template: 
        `<div>
            <principal-component></principal-component>
            <comp-film></comp-film>
        </div>`,
    components: {
        componentesPrincipales,
        componentePeli,
    }
});

//Uso de GET-SET y uso de mensajes, uso d emethods y computed
var componentesPrincipales = Vue.component('principal-component',{
    data(){
        return{
            hola: '',
            mundo: '',
            mensaje: 'mundo'
        }
    },
    template: `
        <div>
            <h1>Hola</h1>
            <p>Párrafo</p>
            <p v-html="hola"></p>
            <p v-html="mundo"></p>
            <p v-pre>{{mundo}}</p>
            <p>{{metodo_prueba | mayusculas |invertir}}</p>
            <label for="nombre1">Texto 1:</label><input type="text" name="nombre2" v-model="hola">
            <label for="nombre2">Texto 1:</label><input type="text" name="nombre2" v-model="mundo">
            <button name="botoncito" @click="mensajeEmergente(this.mensaje)">Push!</button>
        </div>
    `,
    filters: {
        mayusculas(data){
            return data.toUpperCase();
        },
        invertir(v){
            return v.split('').reverse().join('');      
        }
    },
    methods: {
        mensajeEmergente(){
            return alert('Hola :'+this.mensaje);
        }
    },
    computed: {
        metodo_prueba:{
            get(){
                return `***${this.hola} ${this.mundo}***`;
            },
            set(variable){
                var nombre = variable.split(' ');
                this.hola = nombre[0];
                this.apellido = nombre[1];
            }
        }
        
    }    
});
/* El uso de get y set se basa en el uso de atributos computed. Estos se diferencian de los methods debido a que el uso de estos primeros se basa
exclusivamente en cache y cambian solamente si hay cambios tras consultar ello.
get y set con métodos reservados que van dentro de un computed, get = obtener, hace el retorno como se hace normalmente en un método.
Set en cambio hace una preparación de ello a contar de un return de un get(). Se le pasa una variable (que contiene el retorno del get() y en ella se puede resetear los valores
mediante el this.algo.... Se recomienda usar el split para convertir en array el retorno.*/

//Props para padres e hijos, haciendo un for in... 

//Hijo1
/* Si se emplea el .sync explicado en hijo...
Se puede ejecutar el $emit directamente en la propiedad sincronozada, en este caso-ejemplo, en los likes
ANTES:
<label><input type="checkbox" :checked="like" @click="letLike">Me gusta</label> 
Ahora:
 <label><input type="checkbox" :checked="like" @click="$emit('update:like',!like)">Me gusta</label>
 Y así ahorrarse de otros pasos por ejemplo, de programar las funciones. Acá se han dejado ambas alternativas
 Con los like y favoritos y observar que los métodos en el caso de like los comenté
 */
var PropsPeliculas = {
    template:   `<div :id="id">
                    <h3>{{nombre}}</h3>
                    <img v-bind:src="cover"></img>
                    <p>Género: {{clasificacion}}</p>
                    <p v-text="descripcion"></p>
                    <label><input type="checkbox" :checked="like" @click="$emit('update:like',!like)">Me gusta</label>     
                    <button v-text="favorito ?'Favorito':'Agregar a Favs'" v-on:click="letFavorites"></button>      
                </div>`,
    props: {
            id:
            {
                type: Number,
                required: true
            },
            nombre:
            {
                type    : String,
                required: true,
            },
            clasificacion:
            {
                type    : String,
                required: true,
            },
            descripcion:
            {
                type    : String,
                required: false,
                default : 'Sin descripción',
            },
            cover:
            {
                type    : String,
                required: false,
            },
            like: 
            {
                type    : Boolean,
                required: true,
                default: false,
            },
            favorito:
            {
                type    : Boolean,
                required: true,
                default: false,
            }
    },
    watch: {
        favorito (nuevoValor, antiguoValor) {
            return console.log(nuevoValor+'-'+antiguoValor);
        }
    },
    methods:{
        /* letLike: function(){
            var datalike = {
                id:this.id,
                like:!this.like
            };
            //Pasamos la data.... al hijo
            this.$emit('letGusto', datalike);
        }, */
        letFavorites: function(){
            var datafavorita = {
                id:this.id,
                favorito:!this.favorito
            };
            //Pasamos la data.... al hijo
            this.$emit('letFav', datafavorita);
        }
    }

}; 
//Hijo2: Animación del corazón de favorito
var favoritoMensaje = {
    template:   `<div class="movieFav-wrapper">
                    <div class="movieFav" :id="'fav-'+ _uid"></div>
                </div>`,
    //Ciclo de vida de un componente
    //Se puede interactuar con estos elementos con el método $vm0:_uid
    //Este $vm0:_uid es el id no del elemento declarado sino el nodo interno que Vue crea
    beforeCreate(){
        console.log('Antes de crearme');
    },
    created(){
        console.log('Me he creado');
    },
    beforeMount(){
        console.log('Antes de montarse');
    },
    mounted(){
        console.log('Ya montado');
        //debugger
        var vm=this;
        let elemento = document.getElementById(`fav-${this._uid}`); //Obtenemos el elemento por el id de este
        elemento.addEventListener('animationend', function(){
            vm.$emit('hideFav',false);
        });
    },
    beforeUpdated(){
        console.log('Antes de actualizar');
    },
    updated(){
        console.log('Actualizando...');
    },
    beforeDestroy(){
        console.log('Antes de destriurse');
    },
    destroyed(){
        console.log('ya destruido');
    }
};


//Padre
/* Nota: en el template existe una forma alternativa de llamar a los métodos, por ejemplo... en:
@letGusto="setearLike"
se puede eliminar pero en la línea:
:like="movie.like" hay que agregar
:like.sync="movie.like" pero si se hace esto... en el padre hay que modificar lo que se indicará allá
 */
var componentePeli = Vue.component('comp-film',{
    template: `<div>
                <h2>Listado de pelis suculentas</h2>
                <PropsPeliculas v-for="(movie, key) in movies" :key="key" v-bind:id="movie.id" 
                :nombre="movie.nombre" :clasificacion="movie.clasificacion" :descripcion="movie.descripcion" 
                v-bind:cover="movie.cover" :like.sync="movie.like" :favorito="movie.favorito"
                @letFav="setearFavorito" />
                <favoritoMensaje v-if="showFav" v-on:hideFav="onHideFav"/>
            </div>`
    ,
    data(){
        return {
            movies:[
                {
                    id              :  1,
                    nombre          : 'tetanic',
                    clasificacion   : 'drama',
                    descripcion     : 'pelicula mala... pero triste',
                    cover           : 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/zraTDtulFw2wrpyuYf646k95MNq.jpg',
                    like            : false,
                    favorito        : false
                },
                {
                    id              :  2,
                    nombre          : 'buscando a nemo',
                    clasificacion   : 'infantil',
                    descripcion     : 'pelicula de pescados...',
                    cover           : 'https://vignette.wikia.nocookie.net/pixar/images/3/36/Cover_1_1.png/',
                    like            : true,
                    favorito        : false
                },
                {
                    id              :  3,
                    nombre          : 'el dia de la independencia',
                    clasificacion   : 'fantasia',
                    descripcion     : 'el presi de USA pegandole a los alienigenas xd',
                    cover           : 'https://cd.cinescape.com.pe/cinescape-325x457-147324.jpg',
                    like            : false,
                    favorito        : false
                }],
            showFav: false
            }
        },
    
    components: {
        PropsPeliculas,
        favoritoMensaje

    },
    methods: {
/*         setearLike(data) {
            //data viene del padre con los datos pasados id y favorito
            var movieFav= this.movies.find(movie => movie.id==data.id); //Se busca en todas las movies aquella movie (ver el v-for) que coincida con el id...
            movieFav.like = data.like;
        }, */
        setearFavorito(data) {
            //data viene del padre con los datos pasados id y favorito
            var movieFav= this.movies.find(movie => movie.id==data.id); //Se busca en todas las movies aquella movie (ver el v-for) que coincida con el id...
            movieFav.favorito = data.favorito;
            //El efecto...
            this.showFav = movieFav.favorito;
            //Usando el sync no podria usarse para este tipo de alertas
            //No usado pero sirve
            if(movieFav.favorito)
                {
                    alert(movieFav.nombre +' agregada');
                    //En lugar de usar un timeout para ocultar la animacion, se usará parte del ciclo de vida de un componente Vue, ver más arriba en línea 160
                    /* setTimeout(()=>{
                        this.showFav = false;
                        //document.querySelector('.movieFav-wrapper').style.display='none';
                        },1000); */
                }
                
            else
                alert(movieFav.nombre +' no agregada');
        },
        onHideFav(show){
            this.showFav = show;
        }
    }
    
});


//Métodos hijo se usa para hacer eventos interactivos con los hijos de vue. Acá se ejemplifica un botón y un checkbox


/* Los props se utilizando para setear valores imaginando componentes padres e hijos. El componente hijo contiene los datos en detalle y "llama" a su padre
Estructuralmente siempre se debe declarar primero a los hijos y en lugar de la data(), se deben definir estas en props
Construyendo componentes hijos: Dentro de props se declaran los tipos de variable y qué clase tienen
                                Dentro del template se establece el cómo se va a mostrar la data
                                Puede ser una variable o no...
Construyendo componentes padre: Importante declararlos como variables tipo Vue.component
                                El template debe llamar al componente hijo o hijos
                                Esta llamada del componente hijo debe bindear (:) a los props definidos y recorrer la data del componente padre, que tiene el detalle
                                La data se recomienda tenerla a contar de un array de objetos json o array de arrays
                                En components debe LLAMAR a los componente hijo para su uso en el template 
*/

//Declaración obligatoria del objeto Vue
var vm = new Vue({
    el: '#vue-etiqueta'  
});