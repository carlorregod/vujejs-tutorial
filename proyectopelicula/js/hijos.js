'use strict'
//Hijo1
var PropsPeliculas = {
    template:   `<div :id="id" :class="{'favorito': isFav}">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                    <h3>{{nombre}}</h3>
                    <img v-bind:src="cover"></img>
                    <p>idioma: {{clasificacion}}</p>
                    <p v-text="descripcion"></p>
                    <label><input type="checkbox" :checked="like" @click="$emit('update:like',!like)">Me gusta</label>     
                    <button class="btn" :class="btnStatus" v-text="isFav ?'Favorito':'Agregar a Favs'" v-on:click="letFavorites">
                    </button>      
                    <router-link :to="{name:'pelicula', params:{id:id}}" class="btn btn-secondary">Detalles</router-link>
                </div>`,
    props: {
            id:
            {
                type: Number,
                required: true
            },
            nombre:
            {
                type    : String,
                required: true,
            },
            clasificacion:
            {
                type    : String,
                required: false,
            },
            descripcion:
            {
                type    : String,
                required: false,
                default : 'Sin descripción',
            },
            cover:
            {
                type    : String,
                required: false,
            },
            like: 
            {
                type    : Boolean,
                required: true,
                default: false,
            },
            favorito:
            {
                type    : Boolean,
                required: true,
                default: false,
            }
    },
    watch: {
        favorito (nuevoValor, antiguoValor) {
            return console.log(nuevoValor+'-'+antiguoValor);
        }
    },
    computed: {
        btnStatus(){
            return this.isFav ? 'btn-like':'btn-light';
        },
        isFav() {
            let favMovies = this.$store.state.favMovies;
            let index = favMovies.findIndex(movie=>movie.id===this.id);
            return index >= 0;
        }
    }, 
    methods:{

        letFavorites: function(){
            var datafavorita = {
                id:this.id,
                favorito:!this.favorito
            };
            //Pasamos la data.... al hijo
            this.$emit('letFav', datafavorita);
        }
    }

}; 
//Hijo2
var favoritoMensaje = {
    template:   `<div class="movieFav-wrapper">
                    <div class="movieFav" :id="'fav-'+ _uid"></div>
                </div>`,
    //Ciclo de vida de un componente
    //Se puede interactuar con estos elementos con el método $vm0:_uid
    //Este $vm0:_uid es el id no del elemento declarado sino el nodo interno que Vue crea
    beforeCreate(){
        console.log('Antes de crearme');
    },
    created(){
        console.log('Me he creado');
    },
    beforeMount(){
        console.log('Antes de montarse');
    },
    mounted(){
        console.log('Ya montado');
        //debugger
        var vm=this;
        let elemento = document.getElementById(`fav-${this._uid}`); //Obtenemos el elemento por el id de este
        elemento.addEventListener('animationend', function(){
            vm.$emit('hideFav',false);
        });
    },
    beforeUpdated(){
        console.log('Antes de actualizar');
    },
    updated(){
        console.log('Actualizando...');
    },
    beforeDestroy(){
        console.log('Antes de destriurse');
    },
    destroyed(){
        console.log('ya destruido');
    }
};