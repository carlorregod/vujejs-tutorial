'use strict'
//LA centa de db es con skamroty25
//https://www.themoviedb.org/settings/api

var padrePeli = Vue.component('padre-pelis',{
    template: `<div>
                <h2>Listado de pelis suculentas</h2>
                <searchComponent ref="searchComp" v-model="buscar" />
                <!-- Se mostrará si no hay búsquedas -->
                <div v-show="!Object.keys(buscar).length">
                    <h3>Populares {{ counter }}</h3>
                    Data
                    <pre>{{  favMovies  }}</pre>
                    Store
                    <pre>{{ favoritas }}</pre>
                    <button @click="$store.commit('add',add)">Añademe</button>
                    <input type="number" v-model="add"/>
                    <PropsPeliculas v-for="(movie, key) in movies" :key="key" v-bind:id="movie.id" 
                    :nombre="movie.title" :descripcion="movie.overview" :clasificacion="movie.original_language" 
                    v-bind:cover="movie.poster_path | formatoCover" :like.sync="movie.like" :favorito="movie.favorito"
                    @letFav="setearFavorito"/>
                    <div>
                    <button class="btn" :class="{
                        'btn-light'     : n != page,
                        'btn-primary'   : n == page
                        }" v-for="(n,index) in total_pages" :key="index" @click=setPage(n)>{{n}}</button>
                    </div>
                </div>
                <div v-show="Object.keys(buscar).length">
                    <h3>Resultados</h3>
                    <PropsPeliculas v-for="(movie, key) in buscar.results" :key="key" v-if="movie.poster_path" v-bind:id="movie.id" 
                    :nombre="movie.title" :descripcion="movie.overview" :clasificacion="movie.original_language" 
                    v-bind:cover="movie.poster_path | formatoCover" :like.sync="movie.like" :favorito="movie.favorito"
                    @letFav="setearFavorito"/>
                </div>
                <div>
                    <button class="btn" :class="{
                    'btn-light'     : n != buscar.page,
                    'btn-primary'   : n == buscar.page
                    }" v-for="(n,index) in buscar.total_pages" :key="index" @click="$refs.searchComp.setPage(n)">{{n}}</button>
                </div>
                <favoritoMensaje v-if="showFav" v-on:hideFav="onHideFav"/>
            </div>`
    ,
    data(){
        return {
            user:{
                name: 'Carlos',
                lastName: 'Orrego',
                email: 'carlos@orrego.com',
            },
            add:0,
            oldUser: null,
            movies:{},
            showFav: false,
            page: 3,
            total_pages: null,
            buscar:{},
            favMovies: 'Pelis favoritas'
            }
        },
    
    components: {
        PropsPeliculas,
        favoritoMensaje,
        searchComponent

    },
    computed:{
        ...Vuex.mapState({
            favoritas: 'favMovies',
            counter: 'counter'})
    },
    methods: {
        setearFavorito(data) {
            //data viene del padre con los datos pasados id y favorito
            var movieFav= this.movies.find(movie => movie.id==data.id); //Se busca en todas las movies aquella movie (ver el v-for) que coincida con el id...
            movieFav.favorito = data.favorito;
            //Se traslada el mutation que almacenará el favorito
            //this.$store.commit('toogleFavMovie',movieFav);
            this.storeFavorita(movieFav);
            //El efecto...
            this.showFav = movieFav.favorito;
            //Usando el sync no podria usarse para este tipo de alertas
            //No usado pero sirve
            
            if(movieFav.favorito)
                {
                    //En lugar de usar un timeout para ocultar la animacion, se usará parte del ciclo de vida de un componente Vue, ver más arriba en línea 160
                }
                
            else
                alert(movieFav.title +' no agregada');
        },
        onHideFav(show){
            this.showFav = show;
        },
        popularMovies(){
            const URL = `${BASE_URL}discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=${this.page}`;
            fetch(URL)
            .then(response => response.json())
            .then(({results, page, total_pages}) =>{
                this.total_pages = total_pages;
                this.movies = results.map(m=>{
                    m.poster_path = m.poster_path;
                    return m;
                });
            })
        },
        setPage(page){
            const URL = `${BASE_URL}discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=${page}`;
            fetch(URL)
            .then(response => response.json())
            .then(({results, page, total_pages}) =>{
                this.total_pages = total_pages;
                this.page=page;
                this.movies = results.map(m=>{
                    m.poster_path = m.poster_path;
                    return m;
                });
            })
        },
        ...Vuex.mapMutations({
            storeFavorita: 'toogleFavMovie'
        }),

    },
    mounted(){
        let locationURL = new URL(window.location.href);
        this.page=locationURL.searchParams.get('page');
        this.popularMovies();
    }
    
});
