const API_KEY = 'ace07c346f6e94a559b72a74e083c863';
const BASE_URL = 'https://api.themoviedb.org/3/';

Vue.mixin({
    filters: {
        formatoCover(string){
            return `https://image.tmdb.org/t/p/w185_and_h278_bestv2/${string}`;
        }
    },
    data(){
        return{
            apiConfig:`?api_key=${API_KEY}&language=es-CL`,
            apiBaseUrl: `${BASE_URL}`,
        }
    }
});
//Usando vue router
let test = {
    template: `<h1>Hola mundo desde router</h1>`
};
let routes = [
    //Página por defecto
    {
        path: '/',
        name: 'home',
        component: padrePeli,
    },
    //Sólo prueba..
    {
        path: '/test',
        name: 'test',
        component: test,
    },
    //Ruta para la los detalles de las peliculas
    //El path acá necesita pasarle un parámetro que lo identifique y eso se hace con :id
    {
        path: '/pelicula/:id',
        name: 'pelicula',
        component: movieDetails,
    },
];

let routers = new VueRouter({
    //mode: "history",
    linkActiveClass: "active",
    routes: routes,
});
//LLamando a vuex
let store = new Vuex.Store({
    state:{
        counter:0,
        favMovies: []
    },
    mutations:{
        add(state,value){
            if(value>=0)
                state.counter = parseInt(value);
            else
                state.counter = -parseInt(value);
        },
        toogleFavMovie(state,movie){
            //debugger
            let index = state.favMovies.findIndex(favMovie => favMovie.id == movie.id);
            if(index >=0){
                state.favMovies.splice(index,1)
                return;
            }
            state.favMovies.push(movie);
        }
    }
});

//Componente principal
let principal = new Vue({
    el:'#vue-etiqueta',
    //Si se usa router usar esta linea
    router: routers,
    store,
    components: {
        padrePeli
    },
});