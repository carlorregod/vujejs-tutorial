let searchComponent = {
    template: `<div>
    <form class="form-search">
    <div class="input-group">
    <span class="input-group-btn"><button class="btn btn-danger" @click.prevent="clean"><i class="fa fa-eraser fa-lg">&nbsp;</i></button></span>
      <input class="form-control form-text" maxlength="128" placeholder="Buscar..." size="8" type="text" v-model="query"/>
      <span class="input-group-btn"><button class="btn btn-primary" @click.prevent="search"><i class="fa fa-search fa-lg">&nbsp;</i></button></span>
    </div>
  </form>
    </div>
    `,
    data() {
        return {
            query: '',
            page: 1,
        }
    },
    methods: {
        search(){
            let URL = `${BASE_URL}search/movie?api_key=${API_KEY}&query=${this.query}&page=${this.page}`;
            fetch(URL)
            .then(result=>result.json())
            .then(data=>{
                this.$emit('input',data); //Pasamos la data por el evento input que es cuando pasamos la data
            });
        },
        clean(){
            this.query = '';
            this.page=1;
            document.getElementsByClassName('form-text').value='';
            this.$emit('input', {}); //Se pasa un objeto vacío para retornar el elemento anterior
        },
        setPage(n){
            this.page = n;
            this.search();
        }
    },
    
};